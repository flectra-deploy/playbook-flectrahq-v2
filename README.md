 данная установка подразумевает что будет запущено два VPS. 
 На одной машине установлена Ansible, на другой будет установлен FlectraHQ.
 После уствновки FlectraHQ, машину с Ansible удаляем.
 Все инструкции ниже с видео мануалом.

 Перед установкой нужно направить домен на хостинг где будет установлена FlectraHQ.
 После окончания установки будет установлен SSL сертификат Let's Encrypt.

 Установка включает в себя установку всех необходимых серверных компонентов чтобы работать уже в рабочей версии для продакшена.

1. Установка Ansible
https://journal.flectrahq.ru/Ustanovka-Ansible-Ubuntu

2. Подключить уделенную машину, где бует установлена FlectraHQ v.2
https://journal.flectrahq.ru/Podklyuchenie-udalennyh-hostov-SSH-Ansible

3. Установка данного playbook
https://journal.flectrahq.ru/Ustanovka-FlectraHQ-Ansible-playbook

Рекомендуемый хостинг для установки: [Beget](https://beget.com/p786588) 

Время установки. 
Подготовка 10 минут. 
После запуска playbook минут 20.
После заходите в /root/ папку и там в readme все дальнейшие инструкции.